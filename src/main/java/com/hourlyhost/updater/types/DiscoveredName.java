package com.hourlyhost.updater.types;

public class DiscoveredName {
    public String pluginName;
    public float distance;

    public DiscoveredName(String pluginName, float distance) {
        this.pluginName = pluginName;
        this.distance = distance;
    }
}