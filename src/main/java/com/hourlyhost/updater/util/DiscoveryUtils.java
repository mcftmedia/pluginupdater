package com.hourlyhost.updater.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.lucene.search.spell.JaroWinklerDistance;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import com.hourlyhost.updater.client.BukgetClient;
import com.hourlyhost.updater.types.DiscoveredName;
import com.hourlyhost.updater.types.SearchPlugin;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

public class DiscoveryUtils {
    public static String discoverSlug(String search) {
        BukgetClient client = new BukgetClient();

        Gson gson = new Gson();

        Map<String, Object> fields = new HashMap<String, Object>();
        fields.put("size", 1);
        fields.put("sort", "-popularity.monthly");
        fields.put("filters", "[{\"field\":\"plugin_name\",\"action\":\"like\",\"value\":\"" + search + "\"}]");

        HttpResponse<JsonNode> response = client.post("search", fields);

        if (response.getCode() == HttpStatus.SC_OK) {
            SearchPlugin[] plugins = gson.fromJson(response.getBody().toString(), SearchPlugin[].class);

            if (plugins.length > 0) {
                SearchPlugin plugin = plugins[0];

                String slug = plugin.getSlug();

                return slug;
            } else {
                // We should try to find the plugin, BukGet handles spaces
                // oddly, so try a two other variations
                if (search.contains("+")) {
                    return discoverSlug(StringUtils.replace(search, "+", " "));
                } else if (search.contains(" ")) {
                    return discoverSlug(StringUtils.replace(search, " ", "-"));
                }

                throw new FatalSlugSearchException();
            }
        } else {
            throw new FatalSlugSearchException();
        }
    }

    public static DiscoveredName discoverName(PluginManager manager, String pluginName) {
        JaroWinklerDistance jaro = new JaroWinklerDistance();
        String oldPluginName = pluginName;
        float distance = 0;

        for (Plugin loadedPlugin : manager.getPlugins()) {
            String loadedPluginName = loadedPlugin.getDescription().getName();

            float newDistance = jaro.getDistance(oldPluginName, loadedPluginName.toLowerCase());

            if (newDistance > distance) {
                pluginName = loadedPluginName;
                distance = newDistance;
            }
        }

        return new DiscoveredName(pluginName, distance);
    }

    public static SearchPlugin[] search(String search) {
        BukgetClient client = new BukgetClient();

        Gson gson = new Gson();

        Map<String, Object> fields = new HashMap<String, Object>();
        fields.put("sort", "-popularity.monthly");
        fields.put("filters", "[{\"field\":\"plugin_name\",\"action\":\"like\",\"value\":\"" + search + "\"}]");

        HttpResponse<JsonNode> response = client.post("search", fields);

        if (response.getCode() == HttpStatus.SC_OK) {
            SearchPlugin[] plugins = gson.fromJson(response.getBody().toString(), SearchPlugin[].class);
            return plugins;
        } else {
            throw new FatalSearchException();
        }
    }
}
