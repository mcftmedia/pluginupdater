package com.hourlyhost.updater.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import com.hourlyhost.updater.bukkit.ConfigurationManager;
import com.hourlyhost.updater.bukkit.PlayerSession;
import com.hourlyhost.updater.bukkit.PluginReloader;
import com.hourlyhost.updater.bukkit.UpdaterPlugin;
import com.hourlyhost.updater.bukkit.commands.InstallCommand;
import com.hourlyhost.updater.client.Loggly;
import com.hourlyhost.updater.types.ListedVersion;
import com.hourlyhost.updater.types.resource.JAR;
import com.hourlyhost.updater.types.resource.Resource;
import com.hourlyhost.updater.types.resource.Zip;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;

public class PluginDownloader {
    public final static boolean download(URL url, String fileName) throws IOException {
        if (url == null) {
            return false;
        }

        ReadableByteChannel channel = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream("plugins" + File.separator + fileName);
        fos.getChannel().transferFrom(channel, 0, 1 << 24);
        fos.close();

        return true;
    }

    public static boolean downloadVersion(UpdaterPlugin plugin, CommandContext args, CommandSender sender, PlayerSession session, int choice, String action)
            throws CommandException {
        if (session.getVersions() != null) {
            // Get previously processes plugin versions
            List<ListedVersion> versions = session.getVersions();

            if (versions != null && choice >= 0 && versions.get(choice) != null) {
                Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();

                // Get the chosen plugin version
                ListedVersion version = versions.get(choice);

                // Verify that dependencies are met
                List<String> hardDependencies = version.getHardDependencies();
                List<String> softDependencies = version.getSoftDependencies();

                for (Plugin loadedPlugin : loadedPlugins) {
                    String pl = loadedPlugin.getName();

                    hardDependencies.remove(pl);
                    softDependencies.remove(pl);
                }

                // Interrupt when a hard dependency is not fulfilled
                if (hardDependencies.size() > 0) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error: Missing hard dependencies. " + StringUtils.join(hardDependencies, ", "));
                    return false;
                }

                // Warn when a soft dependency is not fulfilled
                if (softDependencies.size() > 0) {
                    sender.sendMessage(ChatColor.RED + "Warning: Missing soft dependencies. " + StringUtils.join(softDependencies, ", "));
                }

                // Install the requested version of a plugin
                PluginReloader reloader = new PluginReloader();
                if (action.equals("install")) {
                    sender.sendMessage(ChatColor.GOLD + "Installing " + session.getPluginName() + ", version " + version.getVersion() + ".");
                } else if (action.equalsIgnoreCase("update")) {
                    sender.sendMessage(ChatColor.GOLD + "Updating " + session.getPluginName() + " to version " + version.getVersion() + ".");
                }

                try {
                    // Unload the old plugin if there is one
                    if (action.startsWith("update")) {
                        reloader.unloadPlugin(session.getPluginName());
                    }

                    // Download the file
                    URL url = new URL(version.getDownload());

                    if (url.getFile().toLowerCase().endsWith(".jar")) {
                        Resource resource = new JAR(session.getPluginName(), version.getFilename(), url);
                        resource.download();
                    } else if (url.getFile().toLowerCase().endsWith(".zip")) {
                        Resource resource = new Zip(session.getPluginName(), version.getFilename(), url);
                        resource.download();
                    } else {
                        sender.sendMessage("Unrecognized file format.");
                        Loggly.log(action, session.getPluginName(), version.getVersion(), "Unrecognized file format: " + url.getFile().toLowerCase());
                        return false;
                    }
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.RED + "A fatal error occurred when downloading that file. Report sent to developers.");
                    Loggly.log(action, session.getPluginName(), version.getVersion(), ExceptionUtils.getStackTrace(e));
                    return false;
                }

                // Load the new plugin file
                try {
                    reloader.loadPlugin(session.getPluginName());
                } catch (Exception e) {
                    if (action.equals("install")) {
                        sender.sendMessage(ChatColor.RED + "Error installing \"" + session.getPluginName() + "\". Report sent to developers.");
                    } else if (action.equalsIgnoreCase("update")) {
                        sender.sendMessage(ChatColor.RED + "Error updating \"" + session.getPluginName() + "\". Report sent to developers.");
                    }
                    Loggly.log(action, session.getPluginName(), version.getVersion(), ExceptionUtils.getStackTrace(e));
                    return false;
                }

                if (action.equals("install")) {
                    sender.sendMessage(ChatColor.YELLOW + session.getPluginName() + " has been successfully installed.");
                } else if (action.equalsIgnoreCase("update")) {
                    sender.sendMessage(ChatColor.YELLOW + session.getPluginName() + " has been successfully updated.");
                }

                if (version.getFilename().toLowerCase().endsWith(".zip")) {
                    ConfigurationManager config = plugin.getGlobalStateManager();
                    String checksum = version.getMD5();
                    config.setPluginChecksum(session.getPluginName(), checksum);
                }

                Loggly.log(action, session.getPluginName(), version.getVersion(), null);
            } else {
                sender.sendMessage(ChatColor.RED + "There is no version with that number.");
            }

            return true;
        }

        if (action.equals("install")) {
            if (session.getSearchResults() != null) {
                String slug = session.getSearchResults()[choice].getSlug();
                args = new CommandContext(args.getSlice(0)[0] + " " + slug);

                InstallCommand command = new InstallCommand(plugin);
                command.install(args, sender);

                return true;
            }
        }

        return true;
    }

    public static String getFilename(Plugin loadedPlugin) {
        String plugin = loadedPlugin.getDescription().getName();

        Class<? extends Plugin> pluginClass = Bukkit.getPluginManager().getPlugin(plugin).getClass();
        URL location = pluginClass.getProtectionDomain().getCodeSource().getLocation();

        return new File(location.getFile()).getName();
    }
}
