package com.hourlyhost.updater.client;

import java.util.Map;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

public class BukgetClient {

    public HttpResponse<JsonNode> response = null;

    private static String resource = "http://api.bukget.org/3/";

    public BukgetClient() {
        HttpClientBuilder clientBuilder = HttpClientBuilder.create();
        clientBuilder.setUserAgent("PluginUpdater");

        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(5000).build();
        clientBuilder.setDefaultRequestConfig(requestConfig);

        Unirest.setHttpClient(clientBuilder.build());
    }

    public HttpResponse<JsonNode> get(String path) {
        try {
            return Unirest.get(resource + path).asJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public HttpResponse<JsonNode> post(String path, Map<String, Object> fields) {
        try {
            return Unirest.post(resource + path).fields(fields).asJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
