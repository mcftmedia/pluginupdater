package com.hourlyhost.updater.client;

import java.util.HashMap;
import java.util.Map;

import com.mashape.unirest.http.Unirest;

public class Loggly {

    private static String resource = "https://logs-01.loggly.com/inputs/01f4bb9a-b981-44e0-91b6-efe3cf2c4159/";

    public static void log(String action, String plugin, String version, String error) {
        Map<String, Object> fields = new HashMap<String, Object>();

        fields.put("action", action);
        fields.put("plugin", plugin);

        if (version != null) {
            fields.put("plugin_version", version);
        }

        if (error != null) {
            fields.put("error", error);
        }

        try {
            Unirest.post(resource).header("Content-Type", "application/x-www-form-urlencoded").fields(fields).asJsonAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
