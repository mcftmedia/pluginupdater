package com.hourlyhost.updater.bukkit;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.bukkit.plugin.Plugin;

import com.sk89q.util.yaml.YAMLFormat;
import com.sk89q.util.yaml.YAMLNode;
import com.sk89q.util.yaml.YAMLProcessor;

public class ConfigurationManager {

    /**
     * Reference to the plugin.
     */
    private UpdaterPlugin plugin;

    /**
     * The global configuration for use when loading worlds
     */
    private YAMLProcessor config;

    /**
     * Enable automatic plugin updates.
     */
    public boolean automaticUpdatesEnabled;

    /**
     * Interval between plugin updates.
     */
    public int automaticUpdateDelay;

    /**
     * Construct the object.
     * 
     * @param plugin
     *            The plugin instance
     */
    public ConfigurationManager(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Load the configuration.
     */
    public void load() {
        // Create the default configuration file
        plugin.saveConfig();

        config = new YAMLProcessor(new File(plugin.getDataFolder(), "config.yml"), true, YAMLFormat.EXTENDED);
        try {
            config.load();
        } catch (IOException e) {
            plugin.getLogger().severe("Error reading configuration for global config: ");
            e.printStackTrace();
        }

        // Remove legacy configuration
        try {
            config.removeProperty("automatic-updates.plugins");
        } catch (NullPointerException e) {
            // Legacy configuration has already been removed
        }

        Map<String, YAMLNode> plugins = config.getNodes("plugin");

        // Add all loaded plugins to the configuration
        Plugin[] loadedPlugins = plugin.getServer().getPluginManager().getPlugins();

        for (Plugin loadedPlugin : loadedPlugins) {
            String pluginName = loadedPlugin.getDescription().getName();

            if (plugins == null || !plugins.containsKey(pluginName)) {
                updatePluginNode(pluginName, false, true, null);
            }
        }

        // Remove any old plugins from the configuration
        if (plugins != null) {
            for (Map.Entry<String, YAMLNode> plugin : plugins.entrySet()) {
                Plugin listedPlugin = this.plugin.getServer().getPluginManager().getPlugin(plugin.getKey());

                if (listedPlugin == null) {
                    config.removeProperty("plugin." + plugin.getKey());
                }
            }
        }

        // Setup the configuration/defaults
        automaticUpdatesEnabled = config.getBoolean("automatic-updates.enabled", false);
        automaticUpdateDelay = config.getInt("automatic-updates.interval.hours", 2) * 72000;

        // Save the configuration
        if (!config.save()) {
            plugin.getLogger().severe("Error saving configuration!");
        }
    }

    /**
     * Unload the configuration.
     */
    public void unload() {
        config.save();
    }

    /**
     * Allow PluginUpdater to interact with a plugin.
     */
    public void enablePlugin(String plugin) {
        config.setProperty("plugin." + plugin + ".enabled", true);
    }

    /**
     * Prevent PluginUpdater from interacting with a plugin.
     */
    public void disablePlugin(String plugin) {
        config.setProperty("plugin." + plugin + ".enabled", false);
    }

    /**
     * Check to see if a plugin can be interacted with.
     */
    public boolean pluginEnabled(String plugin) {
        return config.getBoolean("plugin." + plugin + ".enabled", true);
    }

    /**
     * Check to see if a plugin can't be interacted with.
     */
    public boolean pluginDisabled(String plugin) {
        return !config.getBoolean("plugin." + plugin + ".enabled", true);
    }

    /**
     * Set the plugin's recent file checksum.
     */
    public void setPluginChecksum(String plugin, String checksum) {
        config.setProperty("plugin." + plugin + ".checksum", checksum);
    }

    /**
     * Get a plugin's recent file checksum.
     */
    public String getPluginChecksum(String plugin) {
        return config.getString("plugin." + plugin + ".checksum");
    }

    /**
     * Set the plugin's BukkitDev URL slug.
     */
    public void setPluginSlug(String plugin, String slug) {
        config.setProperty("plugin." + plugin + ".slug", slug);
    }

    /**
     * Get a plugin's BukkitDev URL slug.
     */
    public String getPluginSlug(String plugin) {
        return config.getString("plugin." + plugin + ".slug");
    }

    /**
     * Enable automatic updates for a plugin.
     */
    public void enableAutomaticUpdating(String plugin) {
        config.setProperty("plugin." + plugin + ".automatic", true);
    }

    /**
     * Disable automatic updates for a plugin.
     */
    public void disableAutomaticUpdating(String plugin) {
        config.setProperty("plugin." + plugin + ".automatic", false);
    }

    /**
     * Get a map of plugins that have automatic updating enabled.
     */
    public Map<String, YAMLNode> getAutomaticUpdatingPlugins() {
        Map<String, YAMLNode> automaticPlugins = config.getNodes("plugin");

        for (Map.Entry<String, YAMLNode> plugin : automaticPlugins.entrySet()) {
            boolean automatic = plugin.getValue().getBoolean("automatic", false);

            if (!automatic) {
                automaticPlugins.remove(plugin.getKey());
            }
        }

        return automaticPlugins;
    }

    /**
     * Check to see if a plugin has automatic updating enabled.
     */
    public boolean automaticUpdatesEnabled(String plugin) {
        return config.getBoolean("plugin." + plugin + ".automatic", false);
    }

    private void updatePluginNode(String plugin, boolean automatic, boolean enabled, String slug) {
        String node = "plugin." + plugin;

        config.setProperty(node + ".automatic", automatic);
        config.setProperty(node + ".enabled", enabled);
        if (slug != null) {
            config.setProperty(node + ".slug", slug);
        }
    }
}