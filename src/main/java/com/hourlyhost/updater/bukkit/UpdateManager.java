package com.hourlyhost.updater.bukkit;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpStatus;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.plugin.Plugin;

import com.hourlyhost.updater.client.BukgetClient;
import com.hourlyhost.updater.client.Loggly;
import com.hourlyhost.updater.types.ListedPlugin;
import com.hourlyhost.updater.types.ListedVersion;
import com.hourlyhost.updater.types.resource.JAR;
import com.hourlyhost.updater.types.resource.Resource;
import com.hourlyhost.updater.types.resource.Zip;
import com.hourlyhost.updater.util.Checksum;
import com.hourlyhost.updater.util.DiscoveryUtils;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.sk89q.util.yaml.YAMLNode;

public class UpdateManager implements Runnable {

    private UpdaterPlugin plugin;
    private Logger logger;

    private int updates;
    private int failed;

    /**
     * Construct the object.
     * 
     * @param plugin
     *            The plugin instance
     */
    public UpdateManager(UpdaterPlugin plugin) {
        this.plugin = plugin;
        this.logger = plugin.getLogger();

        this.updates = 0;
        this.failed = 0;
    }

    /**
     * Run the automatic updater task.
     */
    public void run() {
        ConfigurationManager config = plugin.getGlobalStateManager();

        if (!config.automaticUpdatesEnabled) {
            return;
        }

        Map<String, YAMLNode> plugins = config.getAutomaticUpdatingPlugins();

        logger.info(plugin.TAG + "Running automatic updater. Checking " + plugins.size() + " plugins for updates.");

        for (Map.Entry<String, YAMLNode> plugin : plugins.entrySet()) {
            BukgetClient client = new BukgetClient();

            String slug = config.getPluginSlug(plugin.getKey());

            if (slug == null) {
                slug = DiscoveryUtils.discoverSlug(StringUtils.replace(plugin.getKey(), " ", "-").toLowerCase());
                config.setPluginSlug(plugin.getKey(), slug);
            }

            HttpResponse<JsonNode> response = client.get("plugins/bukkit/" + slug);

            if (response.getCode() == HttpStatus.SC_OK) {
                Gson gson = new Gson();

                ListedPlugin listedPlugin = gson.fromJson(response.getBody().toString(), ListedPlugin.class);

                if (listedPlugin == null) {
                    logger.info(this.plugin.TAG + "Encountered error when loading plugin information from BukkitDev.");
                }

                Plugin currentPlugin = Bukkit.getServer().getPluginManager().getPlugin(plugin.getKey());

                String currentVersion = currentPlugin.getDescription().getVersion();
                String latestVersion = listedPlugin.getVersions().get(0).getVersion();

                Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();
                String fileName = null;

                for (Plugin loadedPlugin : loadedPlugins) {
                    if (loadedPlugin.getDescription().getName().equalsIgnoreCase(plugin.getKey())) {
                        Class<? extends Plugin> pluginClass = Bukkit.getPluginManager().getPlugin(plugin.getKey()).getClass();
                        URL location = pluginClass.getProtectionDomain().getCodeSource().getLocation();

                        fileName = new File(location.getFile()).getName();
                    }
                }

                String currentMD5;
                try {
                    currentMD5 = Checksum.getMD5Checksum("plugins" + File.separator + fileName);
                } catch (Exception e) {
                    currentMD5 = null;
                }
                String latestMD5 = listedPlugin.getVersions().get(0).getMD5();

                if (!currentVersion.equals(latestVersion) && !currentMD5.equalsIgnoreCase(latestMD5)) {
                    performUpdate(listedPlugin, plugin.getKey(), fileName, 0);
                    updates++;
                }
            } else {
                logger.info(this.plugin.TAG + "Encountered error when loading BukkitDev.");
            }
        }

        String complete = "Automatic updater complete. " + updates + "/" + plugins.size() + " plugins needed an update.";
        if (failed == 0) {
            logger.info(plugin.TAG + complete);
        } else {
            logger.info(plugin.TAG + complete + " " + failed + " updates failed.");
        }
    }

    private void performUpdate(ListedPlugin listedPlugin, String pluginName, String fileName, int backdate) {
        List<ListedVersion> versions = listedPlugin.getVersions();

        if (versions.size() > 0 && versions.get(backdate) != null) {
            ListedVersion version = versions.get(backdate);

            PluginReloader reloader = new PluginReloader();

            try {
                reloader.unloadPlugin(pluginName);

                URL url = new URL(version.getDownload());

                if (url.getFile().toLowerCase().endsWith(".jar")) {
                    Resource resource = new JAR(pluginName, fileName, url);
                    resource.download();
                } else if (url.getFile().toLowerCase().endsWith(".zip")) {
                    Resource resource = new Zip(pluginName, version.getFilename(), url);
                    resource.download();
                } else {
                    logger.warning(plugin.TAG + "Latest version of \"" + pluginName + "\" had an invalid file format. Will not update.");
                    failed++;
                    return;
                }
            } catch (Exception e) {
                logger.warning(plugin.TAG + "Error downloading update for \"" + pluginName + "\". Report sent to developers.");
                Loggly.log("update", pluginName, version.getVersion(), ExceptionUtils.getStackTrace(e));
                failed++;
                return;
            }

            try {
                reloader.loadPlugin(pluginName);
            } catch (Exception e) {
                logger.warning(plugin.TAG + "Error downloading update for \"" + pluginName + "\", restart may be required. Report sent to developers.");
                Loggly.log("update", pluginName, version.getVersion(), ExceptionUtils.getStackTrace(e));
                failed++;
                return;
            }

            if (version.getFilename().toLowerCase().endsWith(".zip")) {
                ConfigurationManager config = plugin.getGlobalStateManager();
                String checksum = version.getMD5();
                config.setPluginChecksum(pluginName, checksum);
            }

            logger.info(plugin.TAG + "Automatically updated \"" + pluginName + "\" to version " + version.getVersion() + ".");
        } else {
            if (backdate == 0) {
                logger.warning(plugin.TAG + "No files available for \"" + pluginName + "\" for update!");
            } else {
                logger.severe(plugin.TAG + "All attempted backdates failed for \"" + pluginName + "\"!");
                failed++;
            }
        }
    }

}
