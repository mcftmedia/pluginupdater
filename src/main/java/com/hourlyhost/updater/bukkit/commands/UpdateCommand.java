package com.hourlyhost.updater.bukkit.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.http.HttpStatus;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import com.hourlyhost.updater.bukkit.ConfigurationManager;
import com.hourlyhost.updater.bukkit.PlayerSession;
import com.hourlyhost.updater.bukkit.SessionManager;
import com.hourlyhost.updater.bukkit.UpdaterPlugin;
import com.hourlyhost.updater.client.BukgetClient;
import com.hourlyhost.updater.client.Loggly;
import com.hourlyhost.updater.types.DiscoveredName;
import com.hourlyhost.updater.types.ListedPlugin;
import com.hourlyhost.updater.types.ListedVersion;
import com.hourlyhost.updater.util.Checksum;
import com.hourlyhost.updater.util.DiscoveryUtils;
import com.hourlyhost.updater.util.FatalSlugSearchException;
import com.hourlyhost.updater.util.PluginDownloader;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class UpdateCommand {

    private final UpdaterPlugin plugin;
    private PlayerSession session;

    public UpdateCommand(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "update" }, usage = "[plugin]", desc = "Updates a plugin.")
    @CommandPermissions({ "pluginupdater.update" })
    public void update(CommandContext args, CommandSender sender) throws CommandException {
        ConfigurationManager config = plugin.getGlobalStateManager();
        PluginManager manager = plugin.getServer().getPluginManager();

        session = SessionManager.getSession(sender);

        if (args.argsLength() == 0) {
            sender.sendMessage(ChatColor.GOLD + "Are you sure you want to update all plugins (/update confirm)?");
            sender.sendMessage(ChatColor.GRAY + "Enable manual-mode on a plugin to prevent it from being updated (/pu manual [plugin]).");
            session.setTime(new Date().getTime());
            return;
        }

        long delay = (new Date().getTime() - session.getTime()) / 1000 % 60;
        if (args.getString(0).equalsIgnoreCase("confirm") && (delay < 15)) {
            sender.sendMessage(ChatColor.YELLOW + "Enabled plugins are now being updated.");

            UpdateAllPlugins massUpdater = new UpdateAllPlugins(plugin, args, sender);
            Thread massUpdaterThread = new Thread(massUpdater);
            massUpdaterThread.start();

            return;
        }

        // Process a response (e.g. update 3)
        String input = StringUtils.join(args.getSlice(1), " ");
        if (StringUtils.isNumeric(input)) {
            int choice = Integer.parseInt(input) - 1;
            PluginDownloader.downloadVersion(plugin, args, sender, session, choice, "update");
            return;
        }

        String pluginName = StringUtils.join(args.getSlice(1), " ").toLowerCase();
        DiscoveredName name = DiscoveryUtils.discoverName(manager, pluginName);

        // A plugin isn't loaded with that name, inform the user
        if (name.distance < 0.95) {
            sender.sendMessage(ChatColor.RED + "That plugin isn't installed.");
            return;
        } else {
            pluginName = name.pluginName;
        }

        if (config.pluginDisabled(pluginName)) {
            sender.sendMessage(ChatColor.YELLOW + " Updates must be performed manually for this plugin. Use \"/pu enabled true " + pluginName
                    + "\" to enable the plugin.");
            return;
        }

        // Discover the plugin on BukkitDev if it hasn't been yet
        String slug = config.getPluginSlug(pluginName);

        if (slug == null) {
            sender.sendMessage(ChatColor.YELLOW + "Discovering plugin...");

            try {
                slug = DiscoveryUtils.discoverSlug(StringUtils.replace(pluginName, " ", "+"));
            } catch (FatalSlugSearchException e) {
                sender.sendMessage(ChatColor.RED + "Unable to discover Bukkit plugin.");
                Loggly.log("update", pluginName, null, "Discovery failure");
                return;
            }
        }

        // Get the plugin's information
        BukgetClient client = new BukgetClient();
        HttpResponse<JsonNode> response = client.get("plugins/bukkit/" + slug);

        if (response.getCode() == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            ListedPlugin listedPlugin = gson.fromJson(response.getBody().toString(), ListedPlugin.class);

            if (listedPlugin == null) {
                sender.sendMessage(ChatColor.RED + "Unable to discover correct BukkitDev page.");
                Loggly.log("update", pluginName, null, "Discovery failure: " + slug);
                return;
            }

            Plugin pl = manager.getPlugin(pluginName);

            Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();
            String fileName = null;

            for (Plugin loadedPlugin : loadedPlugins) {
                if (loadedPlugin.getDescription().getName().equalsIgnoreCase(pluginName)) {
                    fileName = PluginDownloader.getFilename(loadedPlugin);
                }
            }

            String currentVersion = pl.getDescription().getVersion();
            String latestVersion = listedPlugin.getVersions().get(0).getVersion();

            String currentMD5;
            try {
                currentMD5 = Checksum.getMD5Checksum("plugins" + File.separator + fileName);
            } catch (Exception e) {
                currentMD5 = null;
            }
            String latestMD5 = listedPlugin.getVersions().get(0).getMD5();

            if (currentVersion.equals(latestVersion) && currentMD5.equalsIgnoreCase(latestMD5)) {
                sender.sendMessage(ChatColor.YELLOW + listedPlugin.getPluginName() + " is up to date!");
                sender.sendMessage(ChatColor.GOLD + "Running version " + currentVersion + ". MD5 hash verified with latest.");
            } else {
                // Display the plugin name and description (if available)
                String descriptionPrefix = ChatColor.YELLOW + listedPlugin.getPluginName() + ": ";
                if (!listedPlugin.getDescription().isEmpty()) {
                    String description = listedPlugin.getDescription().replace("\n", "").replace("\r", "");

                    if (description.length() <= 256) {
                        sender.sendMessage(descriptionPrefix + ChatColor.GRAY + description);
                    }
                } else {
                    sender.sendMessage(descriptionPrefix);
                }

                // Prompt the user with the plugin's recent files
                sender.sendMessage(ChatColor.GOLD + "Select a recent file to update to (" + ChatColor.YELLOW + "/update #" + ChatColor.GOLD + "):");

                List<ListedVersion> versions = listedPlugin.getVersions();
                List<ListedVersion> sessionVersions = new ArrayList<ListedVersion>();

                int versionCount = versions.size();

                if (versionCount > 5) {
                    versionCount = 5;
                }

                // Display individual file information
                for (int v = 0; v < versionCount; v++) {
                    ListedVersion version = versions.get(v);

                    sessionVersions.add(version);

                    String number = ChatColor.GOLD + " " + String.valueOf(v + 1) + ". ";
                    String date = DateFormatUtils.ISO_DATE_FORMAT.format(new Date(version.getDate() * 1000));

                    String pluginFile = number;
                    pluginFile += ChatColor.YELLOW + version.getVersion();
                    pluginFile += ChatColor.GRAY + " " + date;
                    pluginFile += ChatColor.ITALIC + " (" + version.getType();
                    pluginFile += " - " + StringUtils.join(version.getGameVersions(), ", ") + ")";

                    sender.sendMessage(pluginFile);
                }

                if (!listedPlugin.getPluginName().isEmpty()) {
                    pluginName = listedPlugin.getPluginName();
                }

                config.setPluginSlug(pluginName, slug);

                session.setVersions(sessionVersions);
                session.setPluginName(pluginName);
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Connection error while connecting to BukkitDev. Try again shortly.");
        }
    }

    public class UpdateAllPlugins implements Runnable {

        public UpdaterPlugin plugin;
        public CommandContext args;
        public CommandSender sender;

        public UpdateAllPlugins(UpdaterPlugin plugin, CommandContext args, CommandSender sender) {
            this.plugin = plugin;
            this.args = args;
            this.sender = sender;
        }

        public void run() {
            ConfigurationManager config = plugin.getGlobalStateManager();
            Plugin[] loadedPlugins = Bukkit.getServer().getPluginManager().getPlugins();

            List<String> updatedPlugins = new ArrayList<String>();
            int updates = 0;
            for (Plugin loadedPlugin : loadedPlugins) {
                String pluginName = loadedPlugin.getName();

                // Plugin is disabled by configuration, skip it
                if (config.pluginDisabled(pluginName)) {
                    continue;
                }

                BukgetClient client = new BukgetClient();

                // Discover the plugin on BukkitDev if it hasn't been yet
                String slug = config.getPluginSlug(pluginName);

                if (slug == null) {
                    try {
                        slug = DiscoveryUtils.discoverSlug(StringUtils.replace(pluginName, " ", "+"));
                    } catch (FatalSlugSearchException e) {
                        Loggly.log("update", pluginName, null, "Discovery failure");
                        continue;
                    }
                }

                HttpResponse<JsonNode> response = client.get("plugins/bukkit/" + slug);

                if (response.getCode() == HttpStatus.SC_OK) {
                    Gson gson = new Gson();

                    ListedPlugin listedPlugin = gson.fromJson(response.getBody().toString(), ListedPlugin.class);

                    if (listedPlugin == null) {
                        continue;
                    }

                    Plugin pl = Bukkit.getServer().getPluginManager().getPlugin(pluginName);
                    ListedVersion latestRelease = listedPlugin.getVersions().get(0);

                    String currentVersion = pl.getDescription().getVersion();
                    String latestVersion = latestRelease.getVersion();

                    String currentMD5 = null;

                    if (latestRelease.getFilename().toLowerCase().endsWith(".zip")) {
                        String checksum = config.getPluginChecksum(pluginName);
                        if (checksum != null) {
                            currentMD5 = checksum;
                        }
                    }

                    if (currentMD5 == null) {
                        try {
                            String fileName = PluginDownloader.getFilename(pl);
                            currentMD5 = Checksum.getMD5Checksum("plugins" + File.separator + fileName);
                        } catch (Exception e) {
                            currentMD5 = null;
                        }
                    }

                    String latestMD5 = latestRelease.getMD5();

                    if (!currentVersion.equals(latestVersion) && !currentMD5.equalsIgnoreCase(latestMD5)) {
                        updates++;

                        try {
                            PluginDownloader.downloadVersion(plugin, args, sender, session, 0, "update");
                            updatedPlugins.add(pluginName);
                        } catch (Exception e) {
                            // Downloader will safely handle any errors.
                        }
                    }
                }
            }

            sender.sendMessage(ChatColor.YELLOW + "All plugins have been updated to their latest versions. " + updates + " updates made:");
            sender.sendMessage(ChatColor.GRAY + "- " + StringUtils.join(updatedPlugins, ", "));
        }
    }
}
