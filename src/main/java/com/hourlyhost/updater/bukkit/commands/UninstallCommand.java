package com.hourlyhost.updater.bukkit.commands;

import java.io.File;
import java.net.URL;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import com.hourlyhost.updater.bukkit.ConfigurationManager;
import com.hourlyhost.updater.bukkit.PluginReloader;
import com.hourlyhost.updater.bukkit.UpdaterPlugin;
import com.hourlyhost.updater.client.Loggly;
import com.hourlyhost.updater.types.DiscoveredName;
import com.hourlyhost.updater.util.DiscoveryUtils;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.NestedCommand;

public class UninstallCommand {

    private final UpdaterPlugin plugin;

    public UninstallCommand(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "pluginupdater", "pu" }, desc = "PluginUpdater admin commands")
    @NestedCommand({ AdminCommands.class })
    public void pluginUpdater(CommandContext args, CommandSender sender) {
    }

    @Command(aliases = { "uninstall", "remove" }, usage = "[plugin]", desc = "Uninstalls a plugin.")
    @CommandPermissions({ "pluginupdater.uninstall" })
    public void uninstall(CommandContext args, CommandSender sender) throws CommandException {
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            sender.sendMessage(ChatColor.YELLOW + "This command is not available on Windows.");
        } else {
            ConfigurationManager config = plugin.getGlobalStateManager();
            PluginManager manager = plugin.getServer().getPluginManager();

            String pluginName = StringUtils.join(args.getSlice(1), " ").toLowerCase();
            DiscoveredName name = DiscoveryUtils.discoverName(manager, pluginName);

            // The input was too messy, inform the user of the closest match
            if (name.distance <= 0.95) {
                sender.sendMessage(ChatColor.YELLOW + "Did you mean \"" + name.pluginName + "\"?");
                return;
            } else {
                pluginName = name.pluginName;
            }

            if (config.pluginDisabled(pluginName)) {
                sender.sendMessage(ChatColor.YELLOW + " This plugin must be uninstalled manually. Use \"/pu enabled true " + pluginName
                        + "\" to enable the plugin.");
                return;
            }

            // Get the plugins class
            Class<? extends Plugin> pluginClass = manager.getPlugin(pluginName).getClass();

            // Unload the plugin
            PluginReloader reloader = new PluginReloader();

            try {
                reloader.unloadPlugin(pluginName);
            } catch (Exception e) {
                sender.sendMessage(ChatColor.RED + "Error unloading \"" + pluginName + "\". Report sent to developers.");
                Loggly.log("unload", pluginName, null, ExceptionUtils.getStackTrace(e));
                return;
            }

            // Get the plugin's path
            URL location = pluginClass.getProtectionDomain().getCodeSource().getLocation();
            String fileName = "plugins" + File.separator + new File(location.getFile()).getName();

            // Rename the file with an added 'old' extension
            // if the user wants the file back in the future
            try {
                FileUtils.moveFile(new File(fileName), new File(fileName + ".old"));
            } catch (FileExistsException e) {
                FileUtils.deleteQuietly(new File(fileName + ".old"));
                if (!softUninstall(sender, fileName, pluginName)) {
                    return;
                }
            } catch (Exception e) {
                sender.sendMessage(ChatColor.RED + "Error soft-uninstalling \"" + pluginName + "\". Unable to create backup of plugin.");
                return;
            }

            sender.sendMessage(ChatColor.YELLOW + "Successfully unloaded and soft-uninstalled \"" + pluginName + "\".");
            Loggly.log("uninstall", pluginName, null, null);
        }
    }

    private boolean softUninstall(CommandSender sender, String fileName, String pluginName) {
        try {
            FileUtils.moveFile(new File(fileName), new File(fileName + ".old"));
        } catch (Exception e) {
            sender.sendMessage(ChatColor.RED + "Error soft-uninstalling \"" + pluginName + "\". Unable to create backup of plugin.");
            return false;
        }

        return true;
    }
}
