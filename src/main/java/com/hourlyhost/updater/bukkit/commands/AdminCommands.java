package com.hourlyhost.updater.bukkit.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;

import com.hourlyhost.updater.bukkit.ConfigurationManager;
import com.hourlyhost.updater.bukkit.UpdaterPlugin;
import com.hourlyhost.updater.types.DiscoveredName;
import com.hourlyhost.updater.util.DiscoveryUtils;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class AdminCommands {

    private final UpdaterPlugin plugin;

    public AdminCommands(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "enabled" }, usage = "[true/false] [plugin]", desc = "Change the manual setting for a plugin.")
    @CommandPermissions({ "pluginupdater.admin" })
    public void manual(CommandContext args, CommandSender sender) throws CommandException {
        if (args.argsLength() <= 1 || !(args.getString(0).equalsIgnoreCase("false") || args.getString(0).equalsIgnoreCase("true"))) {
            sender.sendMessage(ChatColor.RED + "Invalid options. Enter true or false followed by the plugin's name.");
            return;
        }

        ConfigurationManager config = plugin.getGlobalStateManager();
        PluginManager manager = plugin.getServer().getPluginManager();

        String pluginName = StringUtils.join(args.getSlice(2), " ").toLowerCase();
        DiscoveredName name = DiscoveryUtils.discoverName(manager, pluginName);

        // A plugin isn't loaded with that name, inform the user
        if (name.distance < 0.95) {
            sender.sendMessage(ChatColor.RED + "That plugin isn't installed.");
            return;
        } else {
            pluginName = name.pluginName;
        }

        boolean manual = !Boolean.parseBoolean(args.getString(0));

        if (manual) {
            config.disablePlugin(pluginName);
            sender.sendMessage(ChatColor.YELLOW + "\"" + pluginName
                    + "\" can no longer be updated by PluginUpdater. You must do updates for this plugin manually.");
        } else {
            config.enablePlugin(pluginName);
            sender.sendMessage(ChatColor.YELLOW + "\"" + pluginName + "\" can now be updated by PluginUpdater.");
        }
    }

    @Command(aliases = { "autoupdate" }, usage = "[true/false] [plugin]", desc = "Change the autoupdate setting for a plugin.")
    @CommandPermissions({ "pluginupdater.admin" })
    public void autoupdate(CommandContext args, CommandSender sender) throws CommandException {
        if (args.argsLength() <= 1 || !(args.getString(0).equalsIgnoreCase("false") || args.getString(0).equalsIgnoreCase("true"))) {
            sender.sendMessage(ChatColor.RED + "Invalid options. Enter true or false followed by the plugin's name.");
            return;
        }

        ConfigurationManager config = plugin.getGlobalStateManager();
        PluginManager manager = plugin.getServer().getPluginManager();

        String pluginName = StringUtils.join(args.getSlice(2), " ").toLowerCase();
        DiscoveredName name = DiscoveryUtils.discoverName(manager, pluginName);

        // A plugin isn't loaded with that name, inform the user
        if (name.distance < 0.95) {
            sender.sendMessage(ChatColor.RED + "That plugin isn't installed.");
            return;
        } else {
            pluginName = name.pluginName;
        }

        boolean enable = Boolean.parseBoolean(args.getString(0));

        if (enable) {
            String schedule = "Currently running every " + (config.automaticUpdateDelay / 72000) + " hour(s).";
            if (config.automaticUpdatesEnabled(pluginName)) {
                sender.sendMessage(ChatColor.YELLOW + "Already in automatic updater schedule. " + schedule);
            } else {
                config.enableAutomaticUpdating(pluginName);
                sender.sendMessage(ChatColor.YELLOW + "Added to automatic updater schedule. " + schedule);
            }
        } else {
            config.disableAutomaticUpdating(pluginName);
            sender.sendMessage(ChatColor.YELLOW + "Removed from automatic updater schedule.");
        }
    }

    @Command(aliases = { "reload" }, desc = "Reload configuration", max = 0)
    @CommandPermissions({ "pluginupdater.admin" })
    public void reload(CommandContext args, CommandSender sender) throws CommandException {
        try {
            plugin.getGlobalStateManager().unload();
            plugin.getGlobalStateManager().load();
            sender.sendMessage("PluginUpdater configuration reloaded.");
        } catch (Throwable t) {
            sender.sendMessage("Error while reloading: " + t.getMessage());
        }
    }
}
