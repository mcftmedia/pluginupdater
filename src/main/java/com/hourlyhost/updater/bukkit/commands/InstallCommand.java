package com.hourlyhost.updater.bukkit.commands;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.http.HttpStatus;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.plugin.PluginManager;

import com.hourlyhost.updater.bukkit.ConfigurationManager;
import com.hourlyhost.updater.bukkit.PlayerSession;
import com.hourlyhost.updater.bukkit.SessionManager;
import com.hourlyhost.updater.bukkit.UpdaterPlugin;
import com.hourlyhost.updater.client.BukgetClient;
import com.hourlyhost.updater.client.Loggly;
import com.hourlyhost.updater.types.DiscoveredName;
import com.hourlyhost.updater.types.ListedPlugin;
import com.hourlyhost.updater.types.ListedVersion;
import com.hourlyhost.updater.util.DiscoveryUtils;
import com.hourlyhost.updater.util.FatalSlugSearchException;
import com.hourlyhost.updater.util.PluginDownloader;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class InstallCommand {

    private final UpdaterPlugin plugin;
    private PlayerSession session;

    public InstallCommand(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "install" }, usage = "[plugin]", desc = "Install a plugin.")
    @CommandPermissions({ "pluginupdater.update" })
    public void install(CommandContext args, CommandSender sender) throws CommandException {
        ConfigurationManager config = plugin.getGlobalStateManager();
        PluginManager manager = plugin.getServer().getPluginManager();

        session = SessionManager.getSession(sender);

        // Process a response (e.g. install 3)
        String input = StringUtils.join(args.getSlice(1), " ");
        if (StringUtils.isNumeric(input)) {
            int choice = Integer.parseInt(input) - 1;
            PluginDownloader.downloadVersion(plugin, args, sender, session, choice, "install");
            return;
        }

        String pluginName = StringUtils.join(args.getSlice(1), " ").toLowerCase();
        DiscoveredName name = DiscoveryUtils.discoverName(manager, pluginName);

        // A plugin is already loaded with that name, inform the user
        if (name.distance >= 0.95) {
            sender.sendMessage(ChatColor.RED + "That plugin is already installed.");
            return;
        }

        // Discover the plugin on BukkitDev
        sender.sendMessage(ChatColor.YELLOW + "Discovering plugin...");
        BukgetClient client = new BukgetClient();
        String slug = "";

        try {
            slug = DiscoveryUtils.discoverSlug(StringUtils.replace(pluginName, " ", "+"));
        } catch (FatalSlugSearchException e) {
            sender.sendMessage(ChatColor.RED + "Unable to discover Bukkit plugin.");
            Loggly.log("install", pluginName, null, "Discovery failure");
            return;
        }

        // Get the plugin's information
        HttpResponse<JsonNode> response = client.get("plugins/bukkit/" + slug);

        if (response.getCode() == HttpStatus.SC_OK) {
            Gson gson = new Gson();

            ListedPlugin listedPlugin = gson.fromJson(response.getBody().toString(), ListedPlugin.class);

            if (listedPlugin == null) {
                sender.sendMessage(ChatColor.RED + "Unable to discover correct BukkitDev page.");
                Loggly.log("install", pluginName, null, "Discovery failure: " + slug);
                return;
            }

            // Display the plugin name and description (if available)
            String descriptionPrefix = ChatColor.YELLOW + listedPlugin.getPluginName() + ": ";
            if (!listedPlugin.getDescription().isEmpty()) {
                String description = listedPlugin.getDescription().replace("\n", "").replace("\r", "");

                if (description.length() <= 256) {
                    sender.sendMessage(descriptionPrefix + ChatColor.GRAY + description);
                }
            } else {
                sender.sendMessage(descriptionPrefix);
            }

            // Prompt the user with the plugin's recent files
            sender.sendMessage(ChatColor.GOLD + "Select a recent file to install (" + ChatColor.YELLOW + "/install #" + ChatColor.GOLD + "):");

            List<ListedVersion> versions = listedPlugin.getVersions();
            List<ListedVersion> sessionVersions = new ArrayList<ListedVersion>();

            int versionCount = versions.size();

            if (versionCount > 5) {
                versionCount = 5;
            }

            // Display individual file information
            for (int v = 0; v < versionCount; v++) {
                ListedVersion version = versions.get(v);

                sessionVersions.add(version);

                String number = ChatColor.GOLD + " " + String.valueOf(v + 1) + ". ";
                String date = DateFormatUtils.ISO_DATE_FORMAT.format(new Date(version.getDate() * 1000));

                String pluginFile = number;
                pluginFile += ChatColor.YELLOW + version.getVersion(); // Build
                                                                       // version
                pluginFile += ChatColor.GRAY + " " + date; // Build release date
                pluginFile += ChatColor.ITALIC + " (" + version.getType(); // Build
                                                                           // type
                pluginFile += " - " + StringUtils.join(version.getGameVersions(), ", ") + ")"; // Built
                                                                                               // for

                sender.sendMessage(pluginFile);
            }

            if (!listedPlugin.getPluginName().isEmpty()) {
                pluginName = listedPlugin.getPluginName();
            }

            config.setPluginSlug(pluginName, slug);

            session.setVersions(sessionVersions);
            session.setPluginName(pluginName);
        } else {
            sender.sendMessage(ChatColor.RED + "Connection error while connecting to BukkitDev. Try again shortly.");
        }
    }
}
