package com.hourlyhost.updater.bukkit.commands;

import org.bukkit.command.CommandSender;

import com.hourlyhost.updater.bukkit.UpdaterPlugin;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.NestedCommand;

public class GeneralCommands {

    @SuppressWarnings("unused")
    private final UpdaterPlugin plugin;

    public GeneralCommands(UpdaterPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = { "pluginupdater", "pu" }, desc = "PluginUpdater admin commands")
    @NestedCommand({ AdminCommands.class })
    public void pluginUpdater(CommandContext args, CommandSender sender) {
    }
}
