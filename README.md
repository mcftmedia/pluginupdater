# PluginUpdater

[![Build Status](https://drone.io/bitbucket.org/hourlyhost/pluginupdater/status.png)](https://drone.io/bitbucket.org/hourlyhost/pluginupdater/latest)

A Bukkit server plugin for Minecraft that allows for easy plugin updating and installation.

**[Builds](https://drone.io/bitbucket.org/hourlyhost/pluginupdater)** -- **[Download](https://drone.io/bitbucket.org/hourlyhost/pluginupdater/files)** -- **[Bukkit Dev](http://dev.bukkit.org/bukkit-plugins/pluginupdater/)**
